import { Component, OnInit } from '@angular/core';
import AuthSettings = firebase.auth.AuthSettings;
import {AngularFireAuth} from '@angular/fire/auth';
import {Router} from '@angular/router';
import {AuthService} from '../services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    email: string;
    password: string;
    image = 'assets/imges/background.jpg';
  constructor(
    private afAuth: AngularFireAuth,
    private router: Router,
    private authService: AuthService,
              ) { }

  ngOnInit() {
  }
  LoginUser(user) {
   console.log(this.email);
   console.log(this.password);
   this.authService.createUser(this.email, this.password).then(r => this.router.navigate(['/home']));
   // this.afAuth.auth.createUserWithEmailAndPassword(this.email, this.password);


  }
}
