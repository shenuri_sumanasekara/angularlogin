import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../services/auth.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  displayedColumns: string[] = ['uid', 'email'];



  constructor(public auth: AuthService) { }


  ngOnInit() {
    this.auth.getUserData();

  }


}
