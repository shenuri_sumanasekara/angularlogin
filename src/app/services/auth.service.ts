import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { switchMap} from 'rxjs/operators';
import { User } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user$: Observable<any>;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router
  ) {
    this.user$ = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return  of(null);
        }
      })
    );


  }

 async createUser(email, password) {
   const credential = await this.afAuth.auth.createUserWithEmailAndPassword(email, password);
   console.log(email);
   console.log(password);
   return this.updateUserData(credential.user);
   await this.router.navigate(['home']);
 }


   updateUserData({uid, email}: User) {
    console.log('updateUserMtehod');
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${uid}`);

    const data = {
      uid,
      email

    };

    return userRef.set(data, {merge: true});
  }

  getUserData() {
    this.user$ = this.afs.collection(  `users`).valueChanges();
    return this.user$;

  }


}
